# Compute node provisioning
This repository contains provisioning scripts to install additional
software on compute nodes.
One can use [xCAT](https://xcat.org/) or other means to run the script
on one or more nodes.
